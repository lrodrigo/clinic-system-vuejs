// eslint-disable-next-line spaced-comment
/// <reference types="cypress" />
// @ts-check

describe('Transfusions tests', () => {
  beforeEach(() => {
    cy.login('Lorenzo', '1234');
  });

  it('Add new transfusion', () => {
    cy.visit('/');
    cy.contains('Transfusiones').click();
    cy.get('#tab-transfusions___BV_tab_button__').click();
    cy.contains('Agregar Transfusion').click();

    // New transfusion page

    // Type patient
    cy.get('.input-group > .form-control').type('Juan');
    cy.contains('Juan Perez 123456789').click();

    // Establishment
    cy.get('#input-establishment').select('Alvear');

    // Date
    cy.get('.vc-appearance-none').type(`02/02/2020 ${'{enter}'}`);

    // Blood type
    cy.get('#input-blood-type').select('A+');

    // Staying type
    cy.get('#radio-ambulatorio').check({ force: true });
    cy.get('#radio-ambulatorio').click({ force: true });

    // GRD
    cy.get(':nth-child(1) > :nth-child(1) > .b-form-spinbutton > [aria-label="Increment"]').click();
    cy.get('#input-GRD-0').type('10');
    cy.get('#select-GRD-0').select('A+');

    // PF
    cy.get(':nth-child(2) > :nth-child(1) > .b-form-spinbutton > [aria-label="Increment"]').click();
    cy.get(':nth-child(2) > :nth-child(1) > .b-form-spinbutton > [aria-label="Increment"]').click();

    cy.get('#input-PF-0').type('20');
    cy.get('#select-PF-0').select('A+');

    cy.get('#input-PF-1').type('30');
    cy.get('#select-PF-1').select('A+');

    // Platelets
    cy.get(':nth-child(3) > :nth-child(1) > .b-form-spinbutton > [aria-label="Increment"]').click();
    cy.get(':nth-child(3) > :nth-child(1) > .b-form-spinbutton > [aria-label="Increment"]').click();
    cy.get(':nth-child(3) > :nth-child(1) > .b-form-spinbutton > [aria-label="Increment"]').click();

    cy.get('#input-platelets-0').type('40');
    cy.get('#select-platelets-0').select('A+');

    cy.get('#input-platelets-1').type('50');
    cy.get('#select-platelets-1').select('A+');

    cy.get('#input-platelets-2').type('60');
    cy.get('#select-platelets-2').select('A+');

    // Add order
    cy.get('#checkbox-billing-order').check({ force: true });

    // Save
    cy.get('#btn-save-transfusion').click();

    // Order page

    // Staying type
    // cy.get('#radio-ambulatorio').check({ force: true });
    cy.get('[type="radio"').first().check({ force: true });
    cy.get('[type="radio"').first().click({ force: true });

    // Click first preset
    cy.get('#preset-spinbutton-0').click();
    cy.get('#preset-spinbutton-0').type('{uparrow}');

    // Save
    cy.get('#btn-save-order-1').click();

    cy.contains('h3', 'Pedido');
  });
});
