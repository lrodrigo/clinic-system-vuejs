// eslint-disable-next-line spaced-comment
/// <reference types="cypress" />
// @ts-check

describe('Medical studies tests', () => {
  beforeEach(() => {
    cy.login('Lorenzo', '1234');
  });

  it('Add medical study', () => {
    cy.visit('/');
    cy.contains('Estudios').click();
    cy.contains('Agregar Estudio').click();

    // Medical study page

    // Type patient
    cy.get('.input-group > .form-control').type('Juan');
    cy.contains('Juan Perez 123456789').click();

    // Establishment
    cy.get('#input-establishment').select('Alvear');

    // Date
    cy.get('.vc-appearance-none').type(`01/01/2020 ${'{enter}'}`);

    // Medical study
    cy.get('#input-medical-study-other').type('Some medical study');

    // Save
    cy.get('#btn-save-medical-study').click();

    // Order page

    // Staying type
    // cy.get('#radio-ambulatorio').check({ force: true });
    cy.get('[type="radio"').first().check({ force: true });
    cy.get('[type="radio"').first().click({ force: true });

    // Click first preset
    cy.get('#preset-spinbutton-0').click();
    cy.get('#preset-spinbutton-0').type('{uparrow}');

    // Save
    cy.get('#btn-save-order-1').click();

    cy.contains('h3', 'Pedido');
  });

  it('Search medical study by healthcare', () => {
    cy.visit('/');
    cy.contains('Estudios').click();

    // Medical study page

    // Select healthcare
    cy.get('#input-healthcare').select('Healthcare 1');

    // Find
    cy.get('#btn-find-medical-study').click();

    cy.contains('th', 'Fecha', { timeout: 3000 });

    cy.get(':nth-child(1) > :nth-child(5) > #div-dropdown > #div-dropdown__BV_toggle_').click();

    cy.contains('a', 'Ver Estudio');
  });

  it('Edit medical study date', () => {
    cy.visit('/laboratorio/estudios/1');
    cy.contains('Estudio', { timeout: 10000 });

    // Edit date
    cy.get('#icon-edit-date').click();
    cy.get('.vc-appearance-none').clear();
    cy.get('.vc-appearance-none').type(`05/05/2020 ${'{enter}'}`);

    // Save
    cy.get('dd > .btn').click({ force: true });

    cy.contains('div', 'Estudio actualizado', { timeout: 4000 });
    cy.contains('dd', '2020-05-05', { timeout: 4000 });
  });
});
