// eslint-disable-next-line spaced-comment
/// <reference types="cypress" />
// @ts-check

describe('Appointments tests', () => {
  beforeEach(() => {
    cy.login('Lorenzo', '1234');
  });

  let orderUrl = '';

  it('Add new appointment', () => {
    cy.visit('/');
    cy.contains('Nuevo ...').click();

    // Medical study page

    // Type patient
    cy.get('.input-group > .form-control').type('Juan');
    cy.contains('Juan Perez 123456789').click();

    // Establishment
    cy.get('#input-establishment').select('Alvear');

    // Save
    cy.get('#btn-save-appointment').click();

    // Order page

    // Staying type
    cy.get('#radio-ambulatorio').check({ force: true });
    cy.get('#radio-ambulatorio').click({ force: true });

    // Click first preset
    cy.get('#preset-spinbutton-0').click();
    cy.get('#preset-spinbutton-0').type('{uparrow}');

    // Save
    cy.get('#btn-save-order-1').click();

    // Wait for orders page
    cy.location('pathname', { timeout: 60000 })
      .should('not.include', 'nuevo');

    cy.contains('h3', 'Pedido');

    // Get url to use later
    cy.url().then((url) => { orderUrl = url; });
  });

  it('Add new over booking', () => {
    cy.visit('/');
    // Wait for button to be enabled
    cy.wait(2000);
    cy.contains('Nuevo Sobreturno').click();

    // Medical study page

    // Type patient
    cy.get('.input-group > .form-control').type('Juan');
    cy.contains('Juan Perez 123456789').click();

    // Establishment
    cy.get('#input-establishment').select('Alvear');

    // Save
    cy.get('#btn-save-appointment').click();

    // Order page

    // Staying type
    cy.get('#radio-ambulatorio').check({ force: true });
    cy.get('#radio-ambulatorio').click({ force: true });

    // Click first preset
    cy.get('#preset-spinbutton-0').click();
    cy.get('#preset-spinbutton-0').type('{uparrow}');

    // Save
    cy.get('#btn-save-order-1').click();

    // Wait for orders page
    cy.location('pathname', { timeout: 10000 })
      .should('not.include', 'nuevo');

    cy.contains('h3', 'Pedido');
  });

  it('Authorize all practices in order', () => {
    cy.visit(orderUrl);

    // Authorize all practices
    cy.get('.checkbox-practice > input').check({ force: true });

    // Update
    cy.get('#btn-update-order').click();

    cy.contains('div', 'El pedido ha sido actualizado.');
    cy.get('.badge-success').should('be.visible');

    // Unauthorize a practice
    cy.get('.checkbox-practice > input').first().uncheck({ force: true });

    // Update
    cy.get('#btn-update-order').click();
    cy.wait(1000);

    cy.get('.badge-danger', { timeout: 10000 }).should('be.visible');
    cy.contains('div', 'El pedido ha sido actualizado.');

    // Authorize again
    cy.get('.checkbox-practice > input').first().check({ force: true });

    // Update
    cy.get('#btn-update-order').click();

    // Wait for orders page
    cy.get('.badge-success', { timeout: 60000 }).should('be.visible');
  });

  it('Edit appointment', () => {
    cy.visit('/clinica/consultas/1');
    cy.contains('Consulta', { timeout: 10000 });

    // Edit first appointment
    cy.get('#icon-edit-first').click();
    cy.get('#checkbox-first')
      .check({ force: true });

    // Save
    cy.get('dd > .btn').click();

    cy.contains('div', 'Consulta actualizada', { timeout: 4000 });
  });
});
