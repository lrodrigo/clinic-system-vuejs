// eslint-disable-next-line spaced-comment
/// <reference types="cypress" />
// @ts-check

describe('Chemotherapy tests', () => {
  beforeEach(() => {
    cy.login('Lorenzo', '1234');
  });

  it('Add chemotherapy with order', () => {
    cy.visit('/');
    cy.contains('Quimioterapias').click();
    cy.contains('Agregar Quimioterapia').click();

    // Chemotherapy page

    // Type patient
    cy.get('.input-group > .form-control').type('Juan');
    cy.contains('Juan Perez 123456789').click();

    // Establishment
    cy.get('#input-establishment').select('Alvear');

    // Date
    cy.get('.vc-appearance-none').type(`01/01/2020 ${'{enter}'}`);

    // Number of days
    cy.get('#input-number-of-days').type('10');

    // Frequency
    cy.get('#input-frequency').select('Mensual');

    // Billing checkbox
    cy.get('#checkbox-billing-order').check({ force: true });

    // Save
    cy.get('#btn-save-chemotherapy').click();

    // Order page

    // Staying type
    cy.get('#radio-ambulatorio').check({ force: true });
    cy.get('#radio-ambulatorio').click({ force: true });

    // Click first preset
    cy.get('#preset-spinbutton-0').click();
    cy.get('#preset-spinbutton-0').type('{uparrow}');

    // Staying type
    cy.get('#radio-ambulatorio').check({ force: true });
    cy.get('#radio-ambulatorio').click({ force: true });

    // Save
    cy.get('#btn-save-order-1').click();

    cy.contains('h3', 'Pedido');
    cy.contains('dl', 'quimioterapia', { timeout: 10000 });
  });
});
