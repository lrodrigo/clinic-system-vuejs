// eslint-disable-next-line spaced-comment
/// <reference types="cypress" />
// @ts-check

let newPatientId = 0;

describe('Patients tests', () => {
  beforeEach(() => {
    cy.login('Lorenzo', '1234');
  });

  it('Create patient', () => {
    cy.visit('/');
    cy.contains('Pacientes').click();

    cy.contains('Agregar Paciente').click();

    // Patients page

    // Type patient info
    cy.get('.vc-appearance-none').type(`01/01/1990  ${'{enter}'}`);
    cy.get('#input-last-name').type('Rodrigo');
    cy.get('#input-first-name').type('Lorenzo');
    cy.get('#input-dni').type(new Date().getTime().toString());
    cy.get('#input-birth-place > .input-group > .form-control').type('Comodoro');
    cy.get('#input-birth-place > .list-group > .vbst-item').click();
    cy.get('#input-residence-place > .input-group > .form-control').type('Comodoro');
    cy.get('#input-residence-place > .list-group > .vbst-item').click();
    cy.get('#input-phone-number').type('1234567');
    cy.get('#input-email').type('mail@mail.com');
    cy.get('#input-healthcare').select('Healthcare 1');
    cy.get('#input-healthcare-number').type('1234');

    // Gender
    cy.get('[type="radio"').first().check({ force: true });
    cy.get('[type="radio"').first().click({ force: true });

    // Blood type
    cy.get('#input-blood-type').select('A+');

    // Save
    cy.get('#btn-save-patient').click();

    cy.contains('dd', 'Rodrigo, Lorenzo');
    cy.contains('dt', 'Id');

    // Get ID to use later
    cy.get('#dd-patient-id').then(($elem) => {
      newPatientId = parseInt($elem.text(), 10);
    });

    cy.log('Created new patient');
  });

  it('Find patient by last name, first name and DNI', () => {
    cy.visit('/');
    cy.contains('Pacientes').click();

    // Patients page

    // Type patient
    cy.get('#input-first-name').type('Juan');
    cy.get('#input-last-name').type('Perez');
    cy.get('#input-dni').type('123456789');

    // Find
    cy.get('#btn-search-patient').click();

    cy.contains('a', 'Juan');
  });

  it('Find patient by last name', () => {
    cy.visit('/');
    cy.contains('Pacientes').click();

    // Patients page

    // Type patient
    cy.get('#input-last-name').type('Perez');

    // Find
    cy.get('#btn-search-patient').click();

    cy.contains('a', 'Perez');
  });

  it('Find patient by first name', () => {
    cy.visit('/');
    cy.contains('Pacientes').click();

    // Patients page

    // Type patient
    cy.get('#input-first-name').type('Juan');

    // Find
    cy.get('#btn-search-patient').click();

    cy.contains('a', 'Juan');
  });

  it('Find patient by last name, first name and DNI', () => {
    cy.visit('/');
    cy.contains('Pacientes').click();

    // Patients page

    // Type patient
    cy.get('#input-first-name').type('Juan');
    cy.get('#input-last-name').type('Perez');
    cy.get('#input-dni').type('123456789');

    // Find
    cy.get('#btn-search-patient').click();

    cy.contains('a', 'Juan');
  });

  it('Add diagnosis', () => {
    cy.visit(`/pacientes/${newPatientId}`);
    cy.contains('Historia Clinica').click();

    // Add diagnosis
    cy.get('#btn-add-diagnosis').click();

    // Select diagnosis
    cy.get('.typeahead-diagnosis').type('diagnosis 1');
    cy.get('.list-group > .vbst-item').first().click();

    cy.get('#add-diagnosis-modal___BV_modal_footer_ > .btn-primary').click();

    cy.contains('div', 'Diagnostico agregado!', { timeout: 3000 });
    cy.contains('td', 'diagnosis 1');

    cy.log('Diagnosis added');
  });
});
