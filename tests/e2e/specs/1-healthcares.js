// eslint-disable-next-line spaced-comment
/// <reference types="cypress" />
// @ts-check

function addHealthcare(healthcareName, nomenclatorType) {
  cy.visit('/obras_sociales');
  cy.contains('Agregar Obra Social').click();

  // New healthcare page

  // Type name
  cy.get('#input-name').type(healthcareName);

  // Nomenclator
  if (nomenclatorType === 'national') {
    cy.get('#radio-national').check({ force: true });
    cy.get('#radio-national').click({ force: true });
  } else if (nomenclatorType === 'empty') {
    cy.get('#radio-empty').check({ force: true });
    cy.get('#radio-empty').click({ force: true });
  } else {
    // This should be copy from another healthcare
    cy.get('#radio-copy').check({ force: true });
    cy.get('#radio-copy').click({ force: true });
    cy.get('#input-healthcare').select('Particular');
  }

  // Save
  cy.get('#btn-save-healthcare').click();

  cy.contains('dd', healthcareName).should('have.text', healthcareName);
}

describe('Healthcare tests', () => {
  beforeEach(() => {
    cy.login('Lorenzo', '1234');
  });

  it('Add new healthcare with national nomenclator', () => {
    const healthcareName = `Nueva obra social nacional ${new Date().getTime().toString()}`;

    addHealthcare(healthcareName, 'national');
  });

  const emptyHealthcareName = `Nueva obra social vacio ${new Date().getTime().toString()}`;

  it('Add new healthcare with empty nomenclator', () => {
    addHealthcare(emptyHealthcareName, 'empty');
  });

  it('Add practices and value set to healthcare', () => {
    // Healthcare page
    cy.visit('/obras_sociales');
    cy.contains(emptyHealthcareName).click();

    // Nomenclator page
    cy.contains('Nomenclador').click();

    // Code
    cy.get('#input-code-0').type('A123');
    // Description
    cy.get('#input-description-0').type('A description...');
    // Values
    cy.get('#input-fee-0').type('1');
    cy.get('#input-expenses-0').type('2');
    cy.get('#input-biochemistry-0').type('3');

    // More
    cy.get('#btn-more').click();

    // Code
    cy.get('#input-code-1').type('B123');
    // Description
    cy.get('#input-description-1').type('Another description...');
    // Values
    cy.get('#input-fee-1').type('4');
    cy.get('#input-expenses-1').type('5');
    cy.get('#input-biochemistry-1').type('6');

    // More
    cy.get('#btn-more').click();

    // Code
    cy.get('#input-code-2').type('C123');
    // Description
    cy.get('#input-description-2').type('And another description...');
    // Values
    cy.get('#input-fee-2').type('4');
    cy.get('#input-expenses-2').type('5');
    cy.get('#input-biochemistry-2').type('6');

    // Save
    cy.get('#btn-save-practices').click();

    // Wait a second
    cy.wait(1000);

    cy.contains('div', 'Practicas actualizadas');

    // Refresh to check practices were saved
    cy.reload(true);

    cy.get('#input-code-0', { timeout: 6000 }).should('have.value', 'A123');

    // Add value set
    cy.contains('Juegos de Valores').click();

    // Click "Add" button
    cy.get('#btn-add-value-set').click();

    // Set values
    cy.get('#input-fee').type('10');
    cy.get('#input-expenses').type('20');
    cy.get('#input-biochemistry').type('30');

    // Click "Save" button
    cy.get('#btn-save-value-set').click();

    cy.contains('div', 'Juego de Valores agregado!', { timeout: 4000 }).should('have.text', 'Juego de Valores agregado!');
  });
});
