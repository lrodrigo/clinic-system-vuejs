// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('/');
    cy.contains('h2', 'Login');
  });

  it('Login works', () => {
    cy.visit('/');
    cy.get('#input-username').type('Lorenzo');
    cy.get('#input-password').type('1234');

    // Login
    cy.get('#btn-login').click();

    cy.get('#span-username', { timeout: 5000 }).should('have.text', 'Lorenzo');
  });

  it('Login wrong credentials', () => {
    cy.visit('/');
    cy.get('#input-username').type('lrodrigo@gmail.com');
    cy.get('#input-password').type('123123123123123');

    // Login
    cy.get('#btn-login').click();

    cy.get('#div-error-msg', { timeout: 5000 }).should('have.text', ' Usuario o clave incorrecta ');
  });
});
