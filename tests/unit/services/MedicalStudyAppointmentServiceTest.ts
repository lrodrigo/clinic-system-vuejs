import { expect } from 'chai';
import MedicalStudyAppointment from '../../../src/models/MedicalStudyAppointment';
import MedicalStudyAppointmentService from '../../../src/services/MedicalStudyAppointmentsService';
import Patient from '../../../src/models/Patient';

describe('HelloWorld.vue', () => {
  it('renders props.msg when passed', () => {
    const patient = new Patient(1, 'Lorenzo');
    const medicalStudyAppointment = new MedicalStudyAppointment(patient, 200);

    const response = MedicalStudyAppointmentService.create(medicalStudyAppointment)
      .then((data) => console.log(data))
      .catch((error) => console.error(error));

    console.log(response);

    expect(response).to.include('msg');
  });
});
