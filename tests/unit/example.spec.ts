import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Chemotherapies from '@/components/Chemotherapies.vue';

describe('Chemotherapies.vue', () => {
  it('Renders title', () => {
    const title = 'Quimioterapia';
    const wrapper = shallowMount(Chemotherapies, {
      propsData: { },
    });
    expect(wrapper.text()).to.include(title);
  });
});
