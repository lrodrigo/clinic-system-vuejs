import Patient from './Patient';

export default class MedicalStudyAppointment {
    patient: Patient;

    date: string;

    isNightEmergency: boolean;

    medicalStudy: number;

    diagnosis: number;

    attentionType: string;

    room: string;

    doctor: string;

    description: string;

    constructor(patient: Patient, medicalStudy: number) {
      this.medicalStudy = medicalStudy;
      this.patient = patient;
      this.date = '';
      this.isNightEmergency = false;
      this.diagnosis = 0;
      this.attentionType = '';
      this.room = '';
      this.doctor = '';
      this.description = '';
    }

    setDate(date: string) {
      this.date = date;
    }

    toJSON(): object {
      return {
        medicalStudy: this.medicalStudy,
        date: '2010-01-01',
        isNightEmergency: false,
        medicalStudyDescription: `this is new! ${(new Date()).getTime()}`,
      };
    }
}
