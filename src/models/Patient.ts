export default class Patient {
    id: number;

    name: string;

    constructor(id: number, name: string) {
      this.id = id;
      this.name = name;
    }

    greet() {
      return this.name + this.id;
    }
}
