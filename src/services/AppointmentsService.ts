import Repository from './Repository';

const resource = '/appointments';

export default {
  get() {
    return Repository.get(`${resource}`);
  },
  getById(id: number) {
    return Repository.get(`${resource}/${id}`);
  },
  getByClinicAndDate(clinicId: string, date: string) {
    return Repository.get(`${resource}/clinic/${clinicId}/${date}`);
  },
  create(payload: object) {
    return Repository.post(`${resource}`, payload);
  },
  update(id: number, payload: object) {
    return Repository.patch(`${resource}/${id}`, payload);
  },
  delete(id: number) {
    return Repository.delete(`${resource}/${id}`);
  },
  find(params: Record<string, string>) {
    // Eliminate all the null values from the data
    const filteredParams = params;
    Object.keys(filteredParams)
      .forEach((k) => (!filteredParams[k] && filteredParams[k] !== undefined)
      && delete filteredParams[k]);

    const queryString = new URLSearchParams(params).toString();

    if (queryString) {
      return Repository.get(`${resource}/find?${queryString}`);
    }

    // No params
    return Repository.get(`${resource}/find`);
  },
};
