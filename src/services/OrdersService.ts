import Repository from './Repository';

const resource = '/orders';

enum PracticeTypes {
  Consulta = 'consulta',
  Estudio = 'estudio',
  Reserva = 'reserva',
  Transfusion = 'transfusion',
  Quimioterapia = 'quimioterapia',
}

export default {
  get() {
    return Repository.get(`${resource}`);
  },
  getById(id: number) {
    return Repository.get(`${resource}/${id}`);
  },
  getByMedicalPractice(id: number, type: string) {
    return Repository.get(`${resource}/medicalPractice/${type}/${id}`);
  },
  create(payload: object) {
    return Repository.post(`${resource}`, payload);
  },
  update(id: number, payload: object) {
    return Repository.patch(`${resource}/${id}`, payload);
  },
  delete(id: number) {
    return Repository.delete(`${resource}/${id}`);
  },
  find(params: Record<string, string>) {
    // Eliminate all the null values from the data
    const filteredParams = params;
    Object.keys(filteredParams)
      .forEach((k) => (!filteredParams[k] && filteredParams[k] !== undefined)
      && delete filteredParams[k]);

    const queryString = new URLSearchParams(params).toString();

    if (queryString) {
      return Repository.get(`${resource}/find?${queryString}`);
    }

    // No params
    return Repository.get(`${resource}/find`);
  },
  findWithMedicalPractice(params: Record<string, string>) {
    // Eliminate all the null values from the data
    const filteredParams = params;
    Object.keys(filteredParams)
      .forEach((k) => (!filteredParams[k] && filteredParams[k] !== undefined)
      && delete filteredParams[k]);

    const queryString = new URLSearchParams(params).toString();

    if (queryString) {
      return Repository.get(`${resource}/medicalPractice/find?${queryString}`);
    }

    // No params
    return Repository.get(`${resource}/find`);
  },
  findReadyToBill(params: Record<string, string>) {
    // Eliminate all the null values from the data
    const filteredParams = params;
    Object.keys(filteredParams)
      .forEach((k) => (!filteredParams[k] && filteredParams[k] !== undefined)
      && delete filteredParams[k]);

    const queryString = new URLSearchParams(params).toString();

    if (queryString) {
      return Repository.get(`${resource}/find?${queryString}&ready_to_bill=1`);
    }

    // TODO. Handle error, there should always be a query string
    return null;
  },
  findBillingPeriods(params: Record<string, string>) {
    // Eliminate all the null values from the data
    const filteredParams = params;
    Object.keys(filteredParams)
      .forEach((k) => (!filteredParams[k] && filteredParams[k] !== undefined)
      && delete filteredParams[k]);

    const queryString = new URLSearchParams(params).toString();

    if (queryString) {
      return Repository.get(`${resource}/billingPeriods/find?${queryString}`);
    }

    // TODO. Handle error, there should always be a query string
    return null;
  },
  authorize(id: number) {
    return Repository.patch(`${resource}/authorize/${id}`);
  },
  authorizePractices(practices: object) {
    return Repository.post(`${resource}/authorizePractices`, practices);
  },
  getBillingPeriod(id: number) {
    return Repository.get(`${resource}/billingPeriods/${id}`);
  },
  closBillingPeriod(period: object) {
    return Repository.post(`${resource}/closeBillingPeriod`, period);
  },
  updateBillingPeriod(id: number, payload: object) {
    return Repository.patch(`${resource}/billingPeriods/${id}`, payload);
  },
  deleteBillingPeriod(id: number) {
    return Repository.delete(`${resource}/billingPeriods/${id}`);
  },
};
