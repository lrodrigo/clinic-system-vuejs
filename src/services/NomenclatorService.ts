import Repository from './Repository';

const resource = '/nomenclator';

export default {
  getParents() {
    return Repository.get(`${resource}/parents`);
  },
  getChildrenByParentId(parentId: number) {
    return Repository.get(`${resource}/children/${parentId}`);
  },
};
