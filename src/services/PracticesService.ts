import Repository from './Repository';

const presetsResource = '/presets';
const practiceResource = '/practices';
const valueSetResource = '/valueSet';

export default {
  getPreset(id: number) {
    return Repository.get(`${presetsResource}/${id}`);
  },
  getValueSet(id: number) {
    return Repository.get(`${valueSetResource}/${id}`);
  },
  getAllPresets() {
    return Repository.get(`${presetsResource}`);
  },
  createPreset(payload: object) {
    return Repository.post(`${presetsResource}`, payload);
  },
  createValueSet(payload: object) {
    return Repository.post(`${valueSetResource}`, payload);
  },
  updatePreset(id: number, payload: object) {
    return Repository.patch(`${presetsResource}/${id}`, payload);
  },
  updatePractices(payload: object) {
    return Repository.patch(`${practiceResource}`, payload);
  },
  deleteValueSet(id: number) {
    return Repository.delete(`${valueSetResource}/${id}`);
  },
};
