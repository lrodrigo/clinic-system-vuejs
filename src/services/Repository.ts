import axios from 'axios';

const baseDomain = process.env.VUE_APP_API_URL;
const baseURL = `${baseDomain}/api`;

const axiosWrapper = axios.create({
  baseURL,
});

// Add token to headers
axiosWrapper.interceptors.request.use(
  (config) => {
    const newConfig = config;
    const token = localStorage.getItem('token');

    if (token) {
      newConfig.headers.common.Accept = 'application/json';
      newConfig.headers.common.Authorization = `Bearer ${token}`;
    }

    return newConfig;
  },
  (error) => Promise.reject(error),
);

export default axiosWrapper;
