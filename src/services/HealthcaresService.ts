import Repository from './Repository';

const resource = '/healthcares';

export default {
  get() {
    return Repository.get(`${resource}`);
  },
  getById(id: number) {
    return Repository.get(`${resource}/${id}`);
  },
  create(payload: object) {
    return Repository.post(`${resource}`, payload);
  },
  delete(id: number) {
    return Repository.delete(`${resource}/${id}`);
  },
};
