import Repository from './Repository';

const resource = '/clinics';

export default {
  getAll() {
    return Repository.get(`${resource}`);
  },
  getById(id: number) {
    return Repository.get(`${resource}/${id}`);
  },
  create(payload: object) {
    return Repository.post(`${resource}`, payload);
  },
};
