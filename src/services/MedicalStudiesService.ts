import Repository from './Repository';

const resource = '/medicalStudies';

export default {
  get() {
    return Repository.get(`${resource}`);
  },
  getById(id: number) {
    return Repository.get(`${resource}/${id}`);
  },
  create(payload: object) {
    return Repository.post(`${resource}`, payload);
  },
};
