import Repository from './Repository';

const resource = '/patients';

export default {
  get() {
    return Repository.get(`${resource}`);
  },
  getById(id: number) {
    return Repository.get(`${resource}/${id}`);
  },
  create(payload: object) {
    return Repository.post(`${resource}`, payload);
  },
  update(id: number, payload: object) {
    return Repository.patch(`${resource}/${id}`, payload);
  },
  addDiagnosis(patientId: number, diagnosis: object) {
    return Repository.post(`${resource}/${patientId}/diagnosis`, diagnosis);
  },
  getAutocompleteList(query: string) {
    return Repository.get(`${resource}/search?query=${query}`);
  },
  getAutocompleteDiagnosis(query: string) {
    return Repository.get(`${resource}/diagnosis?query=${query}`);
  },
  merge(patient: number, target: number) {
    return Repository.post(`${resource}/merge`, { patient, target });
  },
  delete(id: number) {
    return Repository.delete(`${resource}/${id}`);
  },
  find(params: Record<string, string>) {
    // Eliminate all the null values from the data
    const filteredParams = params;
    Object.keys(filteredParams)
      .forEach((k) => (!filteredParams[k] && filteredParams[k] !== undefined)
      && delete filteredParams[k]);

    const queryString = new URLSearchParams(params).toString();

    if (queryString) {
      return Repository.get(`${resource}/find?${queryString}`);
    }

    // No params
    return Repository.get(`${resource}/find`);
  },
};
