import Repository from './Repository';

const resource = '/logs';

export default {
  find(params: Record<string, string>) {
    // Eliminate all the null values from the data
    const filteredParams = params;
    Object.keys(filteredParams)
      .forEach((k) => (!filteredParams[k] && filteredParams[k] !== undefined)
      && delete filteredParams[k]);

    const queryString = new URLSearchParams(params).toString();

    if (queryString) {
      return Repository.get(`${resource}/find?${queryString}`);
    }

    // No params
    return Repository.get(`${resource}/find`);
  },
};
