import Repository from './Repository';

const resource = '/reports';

export default {
  getAppointments(params: Record<string, string>) {
    return this.getReport('appointments', params);
  },
  getMedicalStudies(params: Record<string, string>) {
    return this.getReport('medicalStudies', params);
  },
  getChemotherapies(params: Record<string, string>) {
    return this.getReport('chemotherapies', params);
  },
  getTransfusions(params: Record<string, string>) {
    return this.getReport('transfusions', params);
  },
  getDiagnosis(params: Record<string, string>) {
    return this.getReport('diagnosis', params);
  },
  getReport(type: string, params: Record<string, string>) {
    // Eliminate all the null values from the data
    const filteredParams = params;
    Object.keys(filteredParams)
      .forEach((k) => (!filteredParams[k] && filteredParams[k] !== undefined)
      && delete filteredParams[k]);

    const queryString = new URLSearchParams(params).toString();

    if (queryString) {
      return Repository.get(`${resource}/${type}?${queryString}`);
    }

    // No params
    return Repository.get(`${resource}/${type}`);
  },
};
