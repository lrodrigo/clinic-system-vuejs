import Repository from './Repository';

const resource = '/users';

export default {
  get() {
    return Repository.get(`${resource}`);
  },
  getById(id: number) {
    return Repository.get(`${resource}/${id}`);
  },
  getAutocompleteList(query: string) {
    return Repository.get(`${resource}/search?query=${query}`);
  },
  create(payload: object) {
    return Repository.post(`${resource}`, payload);
  },
  update(id: number, payload: object) {
    return Repository.patch(`${resource}/${id}`, payload);
  },
  delete(id: number) {
    return Repository.delete(`${resource}/${id}`);
  },
};
