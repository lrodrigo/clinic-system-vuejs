import Repository from './Repository';

export default {
  login(payload: object) {
    return Repository.post('/login', payload);
  },
  register(payload: object) {
    return Repository.post('/register', payload);
  },
};
