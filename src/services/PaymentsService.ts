import Repository from './Repository';

const resource = '/payments';

export default {
  add(id: number, medicalPracticeType: string, payload: object) {
    return Repository.post(`${resource}/${medicalPracticeType}/${id}`, payload);
  },
  delete(id: number) {
    return Repository.delete(`${resource}`);
  },
};
