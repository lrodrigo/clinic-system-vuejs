import Repository from './Repository';

const resource = '/transfusionReservations';

export default {
  get() {
    return Repository.get(`${resource}`);
  },
  getById(id: number) {
    return Repository.get(`${resource}/${id}`);
  },
  create(payload: object) {
    return Repository.post(`${resource}`, payload);
  },
  delete(id: number) {
    return Repository.delete(`${resource}/${id}`);
  },
  update(id: number, payload: object) {
    return Repository.patch(`${resource}/${id}`, payload);
  },
  find(params: Record<string, string>) {
    // Eliminate all the null values from the data
    const filteredParams = params;
    Object.keys(filteredParams)
      .forEach((k) => (!filteredParams[k] && filteredParams[k] !== undefined)
      && delete filteredParams[k]);

    const queryString = new URLSearchParams(params).toString();

    if (queryString) {
      return Repository.get(`${resource}/find?${queryString}`);
    }

    // No params
    return Repository.get(`${resource}/find`);
  },
};
