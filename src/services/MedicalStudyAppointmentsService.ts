import Repository from './Repository';
import MeidcalStudyAppointment from '../models/MedicalStudyAppointment';

const resource = '/medicalStudyAppointments';

interface Parameters {
  page?: string;
  healthcare?: string;
  patient?: string;
  period?: { from: string; to: string };
}

export default {
  find(params: Record<string, string>) {
    // Eliminate all the null values from the data
    const filteredParams = params;
    Object.keys(filteredParams)
      .forEach((k) => (!filteredParams[k] && filteredParams[k] !== undefined)
      && delete filteredParams[k]);

    const queryString = new URLSearchParams(params).toString();

    if (queryString) {
      return Repository.get(`${resource}?${queryString}`);
    }

    // No page
    return Repository.get(`${resource}`);
  },
  getById(id: string) {
    return Repository.get(`${resource}/${id}`);
  },
  create(payload: object) {
    return Repository.post(`${resource}`, payload);
  },
  update(id: number, payload: object) {
    return Repository.patch(`${resource}/${id}`, payload);
  },
  delete(id: number) {
    return Repository.delete(`${resource}/${id}`);
  },
};
