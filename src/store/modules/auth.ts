/* eslint-disable @typescript-eslint/camelcase */
import AuthService from '../../services/AuthService';

interface User {
  username: string;
  password: string;
}

interface AuthState {
  status: string;
  token: string;
  user: string;
  access?: Record<string, string>[];
  // configAccess: string;
  // billingAccess: string;
}

interface AuthSuccessPayload {
  token: string;
  user: { username: string };
  access?: Record<string, string>[];
}

// getters
const getters = {
  isLoggedIn: (state: AuthState) => !!state.token,
  authStatus: (state: AuthState) => state.status,
  username: (state: AuthState) => state.user,
  // eslint-disable-next-line dot-notation
  configAccess: (state: AuthState) => state.access && state.access['config'],
  // eslint-disable-next-line dot-notation
  billingAccess: (state: AuthState) => state.access && state.access['billing'],
};

// actions
const actions = {
  login({ commit }, user: User) {
    return new Promise((resolve, reject) => {
      commit('auth_request');
      AuthService.login(user)
        .then((resp) => {
          if (resp && resp.data && resp.data.success) {
            const { token } = resp.data.success;
            localStorage.setItem('token', token);
            localStorage.setItem('username', user.username);

            // Access rights
            let access = null;
            if (resp.data.access) {
              access = resp.data.access;
              if (resp.data.access.billing) {
                localStorage.setItem('billing', 'true');
              }

              if (resp.data.access.config) {
                localStorage.setItem('config', 'true');
              }
            }

            commit('auth_success', { token, user, access });
            resolve(true);
          }
        })
        .catch((err) => {
          commit('auth_error');
          localStorage.removeItem('token');
          reject(err);
        });
    });
  },
  logout({ commit }) {
    return new Promise((resolve) => {
      commit('logout');
      localStorage.removeItem('token');
      localStorage.removeItem('username');

      resolve();
    });
  },
};

// mutations
const mutations = {
  auth_request(state: AuthState) {
    state.status = 'loading';
  },
  auth_success(state: AuthState, payload: AuthSuccessPayload) {
    state.status = 'success';
    state.token = payload.token;
    state.user = payload.user.username;
    state.access = payload.access;
  },
  auth_error(state: AuthState) {
    state.status = 'error';
  },
  logout(state: AuthState) {
    state.status = '';
    state.token = '';
  },
};

// Initial state
const state = {
  status: '',
  token: localStorage.getItem('token') || '',
  user: localStorage.getItem('username') || '',
  access: {},
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
