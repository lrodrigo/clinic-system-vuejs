import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCalendarAlt, faFlask, faUsers, faTint, faAsterisk, faChartBar,
  faDollarSign, faWrench, faUser, faCalendar, faTimes, faCheck,
  faBan, faMinus, faEye, faPlus, faTrash, faExclamationTriangle,
  faMoon, faCheckCircle, faPrint, faPencilAlt,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VCalendar from 'v-calendar';

// Type ahead library
import VueBootstrapTypeahead from 'vue-bootstrap-typeahead';

// Print HTML
import VueHtmlToPaper from 'vue-html-to-paper';

import Vuex from 'vuex';
import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import Login from './components/Login.vue';

import Appointments from './components/Appointments.vue';
import MedicalStudies from './components/MedicalStudyAppointments.vue';
import Reports from './components/Reports.vue';
import Chemotherapies from './components/Chemotherapies.vue';
import Billings from './components/Billings.vue';
import Transfusions from './components/Transfusions.vue';
import AppointmentsList from './components/AppointmentsList.vue';
import Patients from './components/Patients.vue';
import Patient from './components/Patient.vue';
import Clinic from './components/Clinic.vue';
import Nomenclator from './components/Nomenclator.vue';
import Users from './components/Users.vue';

import Appointment from './components/Appointment.vue';
import MedicalStudyAppointment from './components/MedicalStudyAppointment.vue';
import Chemotherapy from './components/Chemotherapy.vue';
import Transfusion from './components/Transfusion.vue';
import Reservation from './components/Reservation.vue';
import Order from './components/Order.vue';
import Healthcares from './components/Healthcares.vue';
import Establishments from './components/Establishments.vue';
import Clinics from './components/Clinics.vue';
import Preset from './components/Preset.vue';
import Healthcare from './components/Healthcare.vue';
import ValueSet from './components/ValueSet.vue';
import BillingPeriod from './components/BillingPeriod.vue';
import CloseBillingPeriod from './components/CloseBillingPeriod.vue';
import User from './components/User.vue';
import EventLogs from './components/EventLogs.vue';

import NewPatient from './components/NewPatient.vue';
import NewAppointment from './components/NewAppointment.vue';
import NewMedicalStudyAppointment from './components/NewMedicalStudyAppointment.vue';
import NewOrder from './components/NewOrder.vue';
import NewMedicalStudy from './components/NewMedicalStudy.vue';
import NewChemotherapy from './components/NewChemotherapy.vue';
import NewReservation from './components/NewReservation.vue';
import NewTransfusion from './components/NewTransfusion.vue';
import NewBillingPeriod from './components/NewBillingPeriod.vue';
import NewHealthcare from './components/NewHealthcare.vue';
import NewPreset from './components/NewPreset.vue';
import NewEstablishment from './components/NewEstablishmen.vue';
import NewClinic from './components/NewClinic.vue';
import NewValueSet from './components/NewValueSet.vue';
import NewUser from './components/NewUser.vue';

// Vuex store
import store from './store';

import 'es6-promise/auto';

// Add Vuex
Vue.use(Vuex);

// Install BootstrapVue
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

// Calendar
Vue.use(VCalendar);

// Add icons
library.add(faCalendarAlt);
library.add(faFlask);
library.add(faUsers);
library.add(faTint);
library.add(faAsterisk);
library.add(faChartBar);
library.add(faDollarSign);
library.add(faWrench);
library.add(faUser);
library.add(faCalendar);
library.add(faTimes);
library.add(faCheck);
library.add(faTimes);
library.add(faBan);
library.add(faMinus);
library.add(faEye);
library.add(faPlus);
library.add(faTrash);
library.add(faExclamationTriangle);
library.add(faMoon);
library.add(faCheckCircle);
library.add(faPrint);
library.add(faPencilAlt);

Vue.component('font-awesome-icon', FontAwesomeIcon);

// Type ahead
Vue.component('vue-bootstrap-typeahead', VueBootstrapTypeahead);

// Print HTML
const options = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes',
  ],
  styles: [
    'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css',
    // 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
    // './assets/styles/print.css',
  ],
};

Vue.use(VueHtmlToPaper, options);

// 1. Use plugin.
// This installs <router-view> and <router-link>,
// and injects $router and $route to all router-enabled child components
Vue.use(VueRouter);

Vue.config.productionTip = false;

// 3. Create the router
const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: Appointments },
    { path: '/login', component: Login, meta: { guest: true } },
    { path: '/clinica/consultorios/:clinicId(\\d+)/consultas', component: Appointments },
    { path: '/laboratorio/estudios', component: MedicalStudies },
    { path: '/pacientes', component: Patients },
    { path: '/banco/reservas', component: Transfusions },
    { path: '/banco/transfusiones', component: Transfusions },
    { path: '/oncologia/quimioterapias/nueva', component: NewChemotherapy },
    { path: '/oncologia/quimioterapias', component: Chemotherapies },
    { path: '/clinica/consultas', component: AppointmentsList },
    { path: '/reportes/consultas', component: Reports },
    { path: '/reportes/estudios', component: Reports },
    { path: '/reportes/transfusiones', component: Reports },
    { path: '/reportes/quimioterapias', component: Reports },
    { path: '/reportes/diagnosticos', component: Reports },

    // Billing
    { path: '/facturacion/periodos', component: Billings, meta: { billingRequired: true } },
    { path: '/facturacion/pagos', component: Billings, meta: { billingRequired: true } },
    { path: '/facturacion/pedidos', component: Billings, meta: { billingRequired: true } },
    { path: '/facturacion/periodos/nuevo', component: NewBillingPeriod, meta: { billingRequired: true } },
    { path: '/facturacion/periodos/:periodId(\\d+)', component: BillingPeriod, meta: { billingRequired: true } },
    { path: '/facturacion/periodos/:periodId(\\d+)/pedidos', component: BillingPeriod, meta: { billingRequired: true } },
    { path: '/facturacion/periodos/:periodId(\\d+)/resumen', component: BillingPeriod, meta: { billingRequired: true } },
    { path: '/facturacion/periodos/:periodId(\\d+)/detalle', component: BillingPeriod, meta: { billingRequired: true } },
    { path: '/facturacion/periodos/nuevo/pedidos', component: CloseBillingPeriod, meta: { billingRequired: true } },

    { path: '/establecimientos', component: Establishments },
    { path: '/clinica/consultorios', component: Clinics },
    // Normal bookings
    { path: '/clinica/consultas/nueva/consultorio/:clinicId(\\d+)/fecha/:date/hora/:time', component: NewAppointment },
    // Overbookings
    { path: '/clinica/consultas/nueva/consultorio/:clinicId(\\d+)/fecha/:date', component: NewAppointment },
    { path: '/pacientes/nuevo', component: NewPatient },
    { path: '/laboratorio/estudios/nuevo', component: NewMedicalStudyAppointment },
    { path: '/facturacion/pedidos/nuevo/:medicalPracticeType/:medicalPracticeId(\\d+)', component: NewOrder },
    { path: '/laboratorio/estudios/nuevo/paciente/:patientId(\\d+)', component: NewMedicalStudy },
    { path: '/banco/reservas/nueva', component: NewReservation },
    { path: '/banco/transfusiones/nueva', component: NewTransfusion },
    { path: '/pacientes/nuevo', component: NewPatient },
    { path: '/establecimientos/nuevo', component: NewEstablishment },
    { path: '/clinica/consultorios/nuevo', component: NewClinic },

    { path: '/obras_sociales/:healthcareId(\\d+)/valores/nuevo', component: NewValueSet },
    { path: '/obras_sociales/:healthcareId(\\d+)/valores/:valueSetId', component: ValueSet },

    { path: '/pacientes/:patientId(\\d+)', component: Patient },
    { path: '/pacientes/:patientId(\\d+)/historia', component: Patient },
    { path: '/clinica/consultas/:appointmentId(\\d+)', component: Appointment },
    { path: '/laboratorio/estudios/:medicalStudyId(\\d+)', component: MedicalStudyAppointment },
    { path: '/oncologia/quimioterapias/:chemotherapyId(\\d+)', component: Chemotherapy },
    { path: '/banco/transfusiones/:transfusionId(\\d+)', component: Transfusion },
    { path: '/banco/reservas/:reservationId(\\d+)', component: Reservation },
    { path: '/facturacion/pedidos/:orderId(\\d+)', component: Order },
    { path: '/clinica/consultorios/nuevo', component: Clinic },

    { path: '/obras_sociales', component: Healthcares },
    { path: '/obras_sociales/nueva', component: NewHealthcare },
    { path: '/obras_sociales/presets/nuevo', component: NewPreset },
    { path: '/obras_sociales/:healthcareId(\\d+)', component: Healthcare },
    { path: '/obras_sociales/:healthcareId(\\d+)/nomenclador', component: Healthcare },
    { path: '/obras_sociales/:healthcareId(\\d+)/valores', component: Healthcare },
    { path: '/obras_sociales/presets/:presetId(\\d+)', component: Preset },

    { path: '/icd10', component: Nomenclator },

    // Users
    { path: '/usuarios', component: Users, meta: { configRequired: true } },
    { path: '/usuarios/:userId(\\d+)', component: User, meta: { configRequired: true } },
    { path: '/usuarios/nuevo', component: NewUser, meta: { configRequired: true } },

    { path: '/logs', component: EventLogs },
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.guest)) {
    next();
  } else if (!store.getters['auth/isLoggedIn']) {
    next({
      path: '/login',
      query: { redirect: to.fullPath },
    });
  } else if ((to.matched.some((record) => record.meta.configRequired) && !store.getters['auth/configAccess'])
        || (to.matched.some((record) => record.meta.billingRequired) && !store.getters['auth/billingAccess'])) {
    // Check access rights
    next({
      path: '/',
    });
  }
  next(); // make sure to always call next()!
});

new Vue({
  router,
  render: (h) => h(App),
  store,
}).$mount('#app');
